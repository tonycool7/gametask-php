# Brief explanation <h1>
**The task was solved using OOP and SOLID principles** <p>
**Here is a list of notable principles**
* Single Responsibility principle on all classes
* Liskov substitution principle on the ability classes which was used when displaying the beast's abilities in the alvanump generic class
* Open and close principle, which enables us to add more alvanump breeds if necessary, or add abilities or add horn variation and so on
* Also dependency inversion / liskov when we inject the horn object into the constructor which depends on an abstraction not a concrete implementation
* Good naming was also used

*I didn't implement a REST API just yet, because the way I implement my REST API is with the MVC architecture, which will require more code, I can still do it if it is very necessary*

### Brief explanation <h6>
To run the code, navigate to the public folder and run this command.
You can also pass parameter to the api through the url with a GET reqest
```
php -S localhost:30
```

*then run the web app (localhost:30?color=green&height=15&horn=spiral&hair=true) on your browser*