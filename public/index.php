<?php

require '../vendor/autoload.php';

include_once './Router.php';

use \App\Api\V1\ApiController;

$controller = new ApiController();

$router = new Router($controller);

try{
    $router->handle();
}catch (Exception $e){
    echo $e->getMessage();
}
