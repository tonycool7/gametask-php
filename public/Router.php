<?php

use App\Api\V1\Controller;

class Router
{
    private Controller $controller;

    public function __construct(Controller $controller){
        $this->controller = $controller;
    }

    public function handle() : void{

        $verb = strtolower($_SERVER['REQUEST_METHOD']);

        switch ($verb){
            case 'get' :
                $attributes = $_GET;
                http_response_code(200);
                $this->controller->show($attributes);

                break;
            case 'post':
                http_response_code(204);
                $this->controller->store([]);
                break;
            case 'put':
                http_response_code(201);
                $this->controller->update([]);
                break;
            case 'delete':
                http_response_code(204);
                $this->controller->destroy();
                break;
            default:
                throw new Exception("Unrecognized http verb");
                break;
        }
    }


}