<?php


namespace App\GameLibrary\Beasts;

use App\GameLibrary\Beasts\Abilities\{ElectrocuteAbility, SpitPoisonAbility};
use App\GameLibrary\Beasts\Features\Horn;

class BlueAlvanump extends Alvanump
{

    public function __construct(string $color, bool $hair, int $height, Horn $horn)
    {
        $this->setFood(['nuts', 'grapes']);

        $this->setMaxHeight(10);

        parent::__construct($color, $hair, $height, $horn);
    }

}