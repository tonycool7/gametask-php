<?php


namespace App\GameLibrary\Beasts;

use App\GameLibrary\Beasts\Abilities\{GrassGrowthAbility, SpitPoisonAbility};
use App\GameLibrary\Beasts\Features\Horn;

class GreenAlvanump extends Alvanump
{

    public function __construct(string $color, bool $hair, int $height, Horn $horn)
    {
        $this->setFood(['beetles', 'mice', 'nuts']);

        $this->setMinHeight(15);

        $this->setMaxHeight(30);

        parent::__construct($color, $hair, $height, $horn);
    }

}