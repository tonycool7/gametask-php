<?php


namespace App\GameLibrary\Beasts\Abilities;

interface Ability
{
    public function attack() : string;
}