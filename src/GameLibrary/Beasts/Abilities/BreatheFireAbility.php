<?php

namespace App\GameLibrary\Beasts\Abilities;

class BreatheFireAbility implements Ability
{
    public function __construct(){}

    public function attack(): string
    {
        return "Breathe Fire 🔥 on enemy!";
    }
}