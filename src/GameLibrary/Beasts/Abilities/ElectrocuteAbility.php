<?php


namespace App\GameLibrary\Beasts\Abilities;

class ElectrocuteAbility implements Ability
{

    public function __construct(){}

    public function attack(): string
    {
        return "Electrocute enemy";
    }
}