<?php


namespace App\GameLibrary\Beasts\Abilities;

class SpitPoisonAbility implements Ability
{

    public function __construct(){}

    public function attack(): string {
        return "Spit poison on enemy";
    }
}