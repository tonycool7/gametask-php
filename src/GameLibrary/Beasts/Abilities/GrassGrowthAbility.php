<?php

namespace App\GameLibrary\Beasts\Abilities;

class GrassGrowthAbility implements Ability
{

    public function __construct(){}

    public function attack(): string
    {
        return "Accelerate grass growth";
    }
}