<?php

namespace App\GameLibrary\Beasts\Logger;

class BeastIdentificationLogger implements ILogger
{

    public function log(array $messages): void
    {
        echo "<strong style='color: green'>Information saved to log!</strong></br>";
    }
}