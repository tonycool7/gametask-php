<?php

namespace App\GameLibrary\Beasts\Logger;

interface ILogger
{
    public function log(array $messages) : void;
}