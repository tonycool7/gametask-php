<?php


namespace App\GameLibrary\Beasts;

use App\GameLibrary\Beasts\Abilities\Ability;
use App\GameLibrary\Beasts\Exception\BeastException;
use App\GameLibrary\Beasts\Features\Horn;
use App\GameLibrary\Beasts\Logger\BeastIdentificationLogger;
use App\GameLibrary\Beasts\Logger\ILogger;

abstract class Alvanump
{
    private Horn $horn;

    private array $abilities = [];

    private array $food = [];

    private int $height;

    private string $color;

    private bool $hair;

    private int $minHeight = 0;

    private int $maxHeight;

    private ILogger $logger;

    public function __construct(string $color, bool $hair, int $height, Horn $horn){
        $this->color = $color;
        $this->horn = $horn;
        $this->hair = $hair;

        $this->logger = new BeastIdentificationLogger();

        try{
            $this->setHeight($height);
        }catch (\Exception $e){
            echo $e->getMessage().PHP_EOL;
            exit();
        }
    }

    public function addAbility(Ability ...$abilities) : void{
        foreach ($abilities as $ability ){
            array_push($this->abilities, $ability);
        }
    }

    public function displayAbilities() : string{
        $abilities = "";

        foreach ($this->abilities as $ability){
            $abilities .= "<li>{$ability->attack()}</li>";
        }

        return $abilities;
    }

    public function setMinHeight(int $height) : void{
        $this->minHeight = $height;
    }

    public function setHeight(int $height) : void{
        if($height > $this->maxHeight || $height < $this->minHeight){
            throw new BeastException($this->logger, [
                "color" => $this->color,
                "height" => $height,
                "hair" => $this->hair,
                "horn" => $this->horn
                ], 'This height value is out of bound for this type of Alvanumpl');
        }

        $this->height = $height;
    }

    public function getMeals() : string{
        $meals = "";

        foreach ($this->food as $food){
            $meals .= "{$food}, ";
        }

        return $meals;
    }

    public function __toString() : string{
        $meals = $this->getMeals();

        $hornType = $this->horn->display();

        $abilities = $this->displayAbilities();

        return <<< EOT
            <h3>********ALVANUMP FEATURES********</h3>
            <li>color : $this->color</li>
            <li>height : $this->height</li>
            <li>foods : $meals</li>
            <li>horn type : $hornType</li>
            
            <h4>******ALVANUMP ABILITIES*********</h4>
            $abilities
        EOT;
    }

    public function setMaxHeight(int $height) : void{
        $this->maxHeight = $height;
    }

    public function getMinHeight() : int{
        return $this->minHeight;
    }

    public function getMaxHeight() : int{
        return $this->maxHeight;
    }

    public function setFood(array $food) : void{
        $this->food = $food;
    }

}