<?php


namespace App\GameLibrary\Beasts;

use App\GameLibrary\Beasts\Abilities\BreatheFireAbility;
use App\GameLibrary\Beasts\Features\Horn;

class RedAlvanump extends Alvanump
{

    public function __construct(string $color, bool $hair, int $height, Horn $horn)
    {
        $this->setFood(['beetles', 'dragonflies']);

        $this->setMaxHeight(10);

        parent::__construct($color, $hair, $height, $horn);
    }

}