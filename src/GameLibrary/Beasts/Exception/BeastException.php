<?php

namespace App\GameLibrary\Beasts\Exception;

use \App\GameLibrary\Beasts\Logger\ILogger;


class BeastException extends \Exception
{
    private ILogger $logger;

    public function __construct(ILogger $logger, array $data, string $message)
    {
        $this->logger = $logger;

        $this->logger->log($data);

        parent::__construct($message);
    }
}