<?php

namespace App\GameLibrary\Beasts\Features;

abstract class Horn
{
    protected int $totalHorns;

    protected function __construct(int $totalHorns){
        $this->totalHorns = $totalHorns;
    }

    public abstract function display() : string;

}