<?php


namespace App\GameLibrary\Beasts\Features;


class SpiralHorn extends Horn
{

    public function __construct(int $totalHorns)
    {
        parent::__construct($totalHorns);
    }

    public function display() : string
    {
        return "Spiral horn";
    }
}