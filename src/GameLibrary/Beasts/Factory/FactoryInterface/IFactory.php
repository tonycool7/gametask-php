<?php

namespace App\GameLibrary\Beasts\Factory\FactoryInterface;

use App\GameLibrary\Beasts\Alvanump;
use App\GameLibrary\Beasts\Features\Horn;

interface IFactory
{
    public function create(string $color, bool $hair, int $height, Horn $horn) : Alvanump;
}