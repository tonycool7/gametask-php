<?php

namespace App\GameLibrary\Beasts\Factory\FactoryInterface;

use App\GameLibrary\Beasts\Features\Horn;

interface IHorn
{
    public function createHorn(string $type) : Horn;
}