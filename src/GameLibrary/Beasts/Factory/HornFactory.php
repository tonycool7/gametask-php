<?php


namespace App\GameLibrary\Beasts\Factory;

use App\GameLibrary\Beasts\Exception\BeastException;
use App\GameLibrary\Beasts\Factory\FactoryInterface\IHorn;
use App\GameLibrary\Beasts\Logger\ILogger;
use App\GameLibrary\Beasts\Features\{Horn, SpiralHorn, StraightHorn};

class HornFactory implements IHorn
{
    private ILogger $logger;

    public function __construct(ILogger $logger){
        $this->logger = $logger;
    }

    public function createHorn(string $type): Horn
    {
        $horn = null;

        switch ($type){
            case 'straight':
                $horn = new StraightHorn(1);
                break;

            case 'spiral':
                $horn = new SpiralHorn(2);
                break;

            default:
                throw new BeastException($this->logger, [], "<p style='color:red'>This type of horn doesn't exist'</p>");
                break;
        }

        return $horn;
    }
}