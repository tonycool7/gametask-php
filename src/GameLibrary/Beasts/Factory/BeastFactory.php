<?php


namespace App\GameLibrary\Beasts\Factory;

use App\GameLibrary\Beasts\Factory\FactoryInterface\IFactory;
use App\GameLibrary\Beasts\Abilities\{BreatheFireAbility,
    ElectrocuteAbility, GrassGrowthAbility, SpitPoisonAbility};
use App\GameLibrary\Beasts\{Alvanump,
    BlueAlvanump,
    Exception\BeastException,
    GreenAlvanump,
    Logger\ILogger,
    RedAlvanump};
use App\GameLibrary\Beasts\Features\Horn;

class BeastFactory implements IFactory
{
    private ILogger $logger;

    public function __construct(ILogger $logger){
        $this->logger = $logger;
    }

    public function create(string $color, bool $hair, int $height, Horn $horn): Alvanump
    {
        $beast = null;

        switch ($color){
            case 'red' :
                $beast = new RedAlvanump($color, $hair, $height, $horn);

                $beast->addAbility(new BreatheFireAbility());

                $this->logger->log([]);

                break;
            case 'blue' :
                $beast = new BlueAlvanump($color, $hair, $height, $horn);

                $beast->addAbility(new SpitPoisonAbility(), new ElectrocuteAbility());

                $this->logger->log([]);

                break;

            case 'green':
                $beast = new GreenAlvanump($color, $hair, $height, $horn);

                $beast->addAbility(new SpitPoisonAbility(), new GrassGrowthAbility());

                $this->logger->log([]);

                break;
            default:
                $this->logger->log([]);

                throw new BeastException($this->logger, func_get_args(),"<p style='color:red'>Unidentified Beast! 'Beast with these characteristics doesn't exist'</p>");

                break;
        }

        return $beast;
    }
}