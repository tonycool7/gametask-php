<?php

namespace App\Api\V1;

use \App\GameLibrary\Beasts\Factory\{BeastFactory, HornFactory};
use \App\GameLibrary\Beasts\Logger\BeastIdentificationLogger;

class ApiController extends Controller
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){

    }

    public function store(array $attributes){

    }

    public function update(array $attributes)
    {

    }

    public function show(array $attributes)
    {
//        We have to do some kinda validation here, then we can inject the parameters into the factory constructor
        $validateAttributes = $attributes;

//        Extract parameter values
        list("color" => $color, "hair" => $hair, "height" => $height, "horn" => $horn) = $validateAttributes;

        //handle empty query parameters
        $hair = $hair ?? false;
        $height = $height ?? 3;
        $horn = $horn ?? "straight";
        $color = $color ?? "red";

        $logger = new BeastIdentificationLogger();

        try{
            $beastFactory = new BeastFactory($logger);

            $hornFactory = (new HornFactory($logger))->createHorn($horn ?? "");

            $beast = $beastFactory->create($color, $hair, $height, $hornFactory);

            echo $beast;
        }catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    public function destroy(int $id){

    }
}